// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html
import { Hook, HookContext } from '@feathersjs/feathers';

// eslint-disable-next-line @typescript-eslint/no-unused-vars
export default (options = {}): Hook => {
  return async (context: HookContext): Promise<HookContext> => {
    const { data } = context;

    // Throw an error if we didn't get a text
    if(!data.description || !data.quantity ) {
      throw new Error('A thing must have a text');
    }

    // The authenticated user
    const user = context.params.user;
    // The actual message text
    const description = data.description
      // Messages can't be longer than 400 characters
      .substring(0, 400);

    // Override the original data (so that people can't submit additional stuff)
    context.data = {
      description,
      quantity: data.quantity,
      // Set the user id
      userId: user!._id,
    };

    // Best practice: hooks should always return the context
    return context;
  };
};
