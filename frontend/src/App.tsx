import React, { useState, useEffect } from 'react';
import Login from './Login';
import Things from './Things';
import client from './feathers';

const thingsService = client.service('things');
const usersService = client.service('users');

const Application = () => {
  const [login, setLogin] = useState(null);
  const [things, setThings] = useState([]);
  const [users, setUsers] = useState([]);

  useEffect(() => {

    // Try to authenticate with the JWT stored in localStorage
    // TODO:
    client.authenticate().catch(() => {
      setLogin(null);
    });

    // On successfull login
    client.on('authenticated', loginResult => {
      // Get all users and things
      Promise.all([
        thingsService.find(),
        usersService.find(),
      ]).then(([thingPage, userPage]) => {
        // We want the latest things but in the reversed order
        const thingsResult = thingPage.data.reverse();
        const usersResult = userPage.data;

        // Once both return, update the state
        setLogin(loginResult);
        setThings(thingsResult);
        setUsers(usersResult);
      });
    });

    // On logout reset all all local state (which will then show the login screen)
    client.on('logout', () => {
      setLogin(null);
      setThings([]);
      setUsers([]);
    });

    // Add new things to the thing list
    thingsService.on('created', (thing: any) =>
      setThings(currentThings => currentThings.concat(thing))
    );

    // Add new users to the user list
    usersService.on('created', (user: any) =>
      setUsers(currentUsers => currentUsers.concat(user))
    );
  }, []);

  if (login === undefined) {
    return (
      <main className="container text-center">
        <h1>Loading...</h1>
      </main>
    );
  } else if (login) {
    return <Things things={things} users={users} />;
  }

  return <Login />;
};

export default Application;
