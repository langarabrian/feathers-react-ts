import React, { useEffect, useState, useRef, FormEvent } from 'react';
import client from './feathers';

interface User {
  _id: string,
  email: string
};

interface Thing {
  _id: string,
  description: string,
  quantity: number,
  user: User,
  createdAt: number
};

const Things = (props: { users: User[]; things: Thing[] }) => {
  const users = props.users;
  const things = props.things;

  const [description, setDescription] = useState('');
  const [quantity, setQuantity] = useState(1);

  function addThing(ev: FormEvent) {
    const element = ev.currentTarget as HTMLFormElement;
    if ( element.checkValidity()) {
      element.classList.remove('was-validated');
      client
        .service('things')
        .create({ description, quantity })
        .then(() => {
          setDescription('');
          setQuantity(1);
        });
    } else {
      element.classList.add('was-validated');
    }
    ev.preventDefault();
  }

  return (
    <div className="container">
      <main>
        <button className="btn btn-primary btn-lg mt-2"
                onClick={() => client.logout()}
                type="button">Sign out</button>
        <div className="py-5 text-center">
          <h2>Things</h2>
        </div>

        <div className="row g-5">
          <div className="col-md-5 col-lg-4 order-md-last">
            <h4 className="mb-3">
              Users
            </h4>
            {users.map(user => (
              <p>{user.email}</p>
            ))}

          </div>
          <div className="col-md-7 col-lg-8">
            <h4 className="mb-3">
              New thing
            </h4>

            <form className="needs-validation" onSubmit={addThing} noValidate>
              <div className="row g-3">

                <div className="col-sm-10">
                  <label htmlFor="description" className="form-label">Description</label>
                  <input type="text"
                         className="form-control"
                         id="description"
                         placeholder=""
                         value={description}
                         onChange={e => setDescription( e.target.value )}
                         required />
                  <div className="invalid-feedback">
                    Thing description is required.
                  </div>
                </div>


                <div className="col-sm-2">
                  <label htmlFor="quantity" className="form-label">Quantity</label>
                  <input type="number"
                         className="form-control"
                         id="description"
                         placeholder=""
                         value={quantity}
                         onChange={e => setQuantity( parseInt( e.target.value ) )}
                         required />
                  <div className="invalid-feedback">
                    Thing quantity is required.
                  </div>
                </div>

                <div className="col-sm-4">
                  <button className="btn btn-primary btn-lg"
                          type="submit">Add thing</button>
                </div>
              </div>
            </form>
            <table className="table">
              <thead>
                <tr>
                  <th scope="col">Description</th>
                  <th scope="col">Quantity</th>
                  <th scope="col">Added by</th>
                </tr>
              </thead>
              <tbody>
                {things.map(thing => (
                  <tr key={thing._id}>
                    <td>{thing.description}</td>
                    <td>{thing.quantity}</td>
                    <td>{thing.user.email}</td>
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
        </div>

      </main>
    </div>
  );
};

export default Things;
