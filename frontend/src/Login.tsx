import React, { useState } from 'react';
import client from './feathers';

interface Error {
  message: string
};

const Login = () => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [error, setError] = useState<Error>();

  function login() {
    // TODO
    return client
      .authenticate({
        strategy: 'local',
        email,
        password,
      })
      .catch(err => setError(err));
  }

  function signup() {
    return client
      .service('users')
      .create({ email, password })
      .then(() => login());
  }

  return (
    <div className="text-center">
    <main className="form-signin">
      <form>
        <h1 className="h3 mb-3 fw-normal">Please sign up / in</h1>
        <p>{error && error.message}</p>

        <div className="form-floating">
          <input type="email"
                 className="form-control"
                 id="floatingInput"
                 onChange={e => setEmail( e.target.value )}
                 placeholder="name@example.com" />
          <label htmlFor="floatingInput">Email address</label>
        </div>
        <div className="form-floating">
          <input type="password"
                 className="form-control"
                 id="floatingPassword"
                 onChange={e => setPassword( e.target.value )}
                 placeholder="Password" />
          <label htmlFor="floatingPassword">Password</label>
        </div>

        <button className="w-100 btn btn-lg btn-primary mb-2"
                onClick={() => login()}
                type="button">Sign in</button>
        <button className="w-100 btn btn-lg btn-primary"
                onClick={() => signup()}
                type="button">Sign up and sign in</button>
      </form>

    </main>
    </div>
  );
};

export default Login;
